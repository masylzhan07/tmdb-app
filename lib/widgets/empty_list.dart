import 'package:flutter/material.dart';

class CustomEmptyList extends StatelessWidget {
  const CustomEmptyList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Список пуст',
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w400,
      ),
    );
  }
}
