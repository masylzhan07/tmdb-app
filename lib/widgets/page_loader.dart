import 'package:flutter/material.dart';

class CustomPageLoader extends StatelessWidget {
  const CustomPageLoader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Align(
      alignment: Alignment.topCenter,
      child: CircularProgressIndicator(),
    );
  }
}
