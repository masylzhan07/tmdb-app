import 'package:flutter/material.dart';

class CustomPageError extends StatelessWidget {
  const CustomPageError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Align(
      alignment: Alignment.topCenter,
      child: Text('Error'),
    );
  }
}
