import 'dart:async';
import 'package:tmdb_app/data/models/movie.dart';
import 'package:tmdb_app/data/providers/tmdb_api.dart';

class MovieRepository {
  Future<MovieModel> fetchMovie(int movieId) async {
    try {
      final result = await TMDBAPIRepository().fetchMovie(movieId);
      if (result.statusCode == 200) {
        return MovieModel.fromMap(result.data);
      } else {
        throw Exception('Error on fetchMovie: ${result.statusCode}');
      }
    } on Exception catch (e) {
      throw Exception('Error: $e');
    }
  }
}
