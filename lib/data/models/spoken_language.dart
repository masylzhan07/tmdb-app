import 'dart:convert';

class SpokenLanguageModel {
  final String? english_name;
  final String? iso_639_1;
  final String? name;
  SpokenLanguageModel({
    this.english_name,
    this.iso_639_1,
    this.name,
  });

  SpokenLanguageModel copyWith({
    String? english_name,
    String? iso_639_1,
    String? name,
  }) {
    return SpokenLanguageModel(
      english_name: english_name ?? this.english_name,
      iso_639_1: iso_639_1 ?? this.iso_639_1,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'english_name': english_name,
      'iso_639_1': iso_639_1,
      'name': name,
    };
  }

  factory SpokenLanguageModel.fromMap(Map<String, dynamic> map) {
    return SpokenLanguageModel(
      english_name: map['english_name'],
      iso_639_1: map['iso_639_1'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SpokenLanguageModel.fromJson(String source) =>
      SpokenLanguageModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'SpokenLanguageModel(english_name: $english_name, iso_639_1: $iso_639_1, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is SpokenLanguageModel &&
        other.english_name == english_name &&
        other.iso_639_1 == iso_639_1 &&
        other.name == name;
  }

  @override
  int get hashCode =>
      english_name.hashCode ^ iso_639_1.hashCode ^ name.hashCode;
}
