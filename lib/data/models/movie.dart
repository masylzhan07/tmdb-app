import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:tmdb_app/data/models/production.dart';
import 'package:tmdb_app/data/models/spoken_language.dart';
import 'package:tmdb_app/data/models/title.dart';

import 'collection.dart';

class MovieModel {
  final bool? adult;
  final String? backdrop_path;
  final CollectionModel? belongs_to_collection;
  final int? budget;
  final List<int>? genre_ids;
  final List<TitleModel>? genres;
  final String? homepage;
  final int? id;
  final String? imdb_id;
  final String? original_language;
  final String? original_title;
  final String? overview;
  final double? popularity;
  final String? poster_path;
  final List<ProductionCompanyModel>? production_companies;
  final List<ProductionCountryModel>? production_countries;
  final String? release_date;
  final int? revenue;
  final int? runtime;
  final List<SpokenLanguageModel>? spoken_languages;
  final String? status;
  final String? tagline;
  final String? title;
  final bool? video;
  final double? vote_average;
  final int? vote_count;
  MovieModel({
    this.adult,
    this.backdrop_path,
    this.belongs_to_collection,
    this.budget,
    this.genre_ids,
    this.genres,
    this.homepage,
    this.id,
    this.imdb_id,
    this.original_language,
    this.original_title,
    this.overview,
    this.popularity,
    this.poster_path,
    this.production_companies,
    this.production_countries,
    this.release_date,
    this.revenue,
    this.runtime,
    this.spoken_languages,
    this.status,
    this.tagline,
    this.title,
    this.video,
    this.vote_average,
    this.vote_count,
  });

  MovieModel copyWith({
    bool? adult,
    String? backdrop_path,
    CollectionModel? belongs_to_collection,
    int? budget,
    List<int>? genre_ids,
    List<TitleModel>? genres,
    String? homepage,
    int? id,
    String? imdb_id,
    String? original_language,
    String? original_title,
    String? overview,
    double? popularity,
    String? poster_path,
    List<ProductionCompanyModel>? production_companies,
    List<ProductionCountryModel>? production_countries,
    String? release_date,
    int? revenue,
    int? runtime,
    List<SpokenLanguageModel>? spoken_languages,
    String? status,
    String? tagline,
    String? title,
    bool? video,
    double? vote_average,
    int? vote_count,
  }) {
    return MovieModel(
      adult: adult ?? this.adult,
      backdrop_path: backdrop_path ?? this.backdrop_path,
      belongs_to_collection:
          belongs_to_collection ?? this.belongs_to_collection,
      budget: budget ?? this.budget,
      genre_ids: genre_ids ?? this.genre_ids,
      genres: genres ?? this.genres,
      homepage: homepage ?? this.homepage,
      id: id ?? this.id,
      imdb_id: imdb_id ?? this.imdb_id,
      original_language: original_language ?? this.original_language,
      original_title: original_title ?? this.original_title,
      overview: overview ?? this.overview,
      popularity: popularity ?? this.popularity,
      poster_path: poster_path ?? this.poster_path,
      production_companies: production_companies ?? this.production_companies,
      production_countries: production_countries ?? this.production_countries,
      release_date: release_date ?? this.release_date,
      revenue: revenue ?? this.revenue,
      runtime: runtime ?? this.runtime,
      spoken_languages: spoken_languages ?? this.spoken_languages,
      status: status ?? this.status,
      tagline: tagline ?? this.tagline,
      title: title ?? this.title,
      video: video ?? this.video,
      vote_average: vote_average ?? this.vote_average,
      vote_count: vote_count ?? this.vote_count,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'adult': adult,
      'backdrop_path': backdrop_path,
      'belongs_to_collection': belongs_to_collection?.toMap(),
      'budget': budget,
      'genre_ids': genre_ids,
      'genres': genres?.map((x) => x.toMap()).toList(),
      'homepage': homepage,
      'id': id,
      'imdb_id': imdb_id,
      'original_language': original_language,
      'original_title': original_title,
      'overview': overview,
      'popularity': popularity,
      'poster_path': poster_path,
      'production_companies':
          production_companies?.map((x) => x.toMap()).toList(),
      'production_countries':
          production_countries?.map((x) => x.toMap()).toList(),
      'release_date': release_date,
      'revenue': revenue,
      'runtime': runtime,
      'spoken_languages': spoken_languages?.map((x) => x.toMap()).toList(),
      'status': status,
      'tagline': tagline,
      'title': title,
      'video': video,
      'vote_average': vote_average,
      'vote_count': vote_count,
    };
  }

  factory MovieModel.fromMap(Map<String, dynamic> map) {
    return MovieModel(
      adult: map['adult'],
      backdrop_path: map['backdrop_path'],
      belongs_to_collection: map['belongs_to_collection'] != null
          ? CollectionModel.fromMap(map['belongs_to_collection'])
          : null,
      budget: map['budget']?.toInt(),
      genre_ids:
          map['genre_ids'] != null ? List<int>.from(map['genre_ids']) : null,
      genres: map['genres'] != null
          ? List<TitleModel>.from(
              map['genres']?.map((x) => TitleModel.fromMap(x)))
          : null,
      homepage: map['homepage'],
      id: map['id']?.toInt(),
      imdb_id: map['imdb_id'],
      original_language: map['original_language'],
      original_title: map['original_title'],
      overview: map['overview'],
      popularity: map['popularity']?.toDouble(),
      poster_path: map['poster_path'],
      production_companies: map['production_companies'] != null
          ? List<ProductionCompanyModel>.from(map['production_companies']
              ?.map((x) => ProductionCompanyModel.fromMap(x)))
          : null,
      production_countries: map['production_countries'] != null
          ? List<ProductionCountryModel>.from(map['production_countries']
              ?.map((x) => ProductionCountryModel.fromMap(x)))
          : null,
      release_date: map['release_date'],
      revenue: map['revenue']?.toInt(),
      runtime: map['runtime']?.toInt(),
      spoken_languages: map['spoken_languages'] != null
          ? List<SpokenLanguageModel>.from(map['spoken_languages']
              ?.map((x) => SpokenLanguageModel.fromMap(x)))
          : null,
      status: map['status'],
      tagline: map['tagline'],
      title: map['title'],
      video: map['video'],
      vote_average: map['vote_average']?.toDouble(),
      vote_count: map['vote_count']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory MovieModel.fromJson(String source) =>
      MovieModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MovieModel(adult: $adult, backdrop_path: $backdrop_path, belongs_to_collection: $belongs_to_collection, budget: $budget, genre_ids: $genre_ids, genres: $genres, homepage: $homepage, id: $id, imdb_id: $imdb_id, original_language: $original_language, original_title: $original_title, overview: $overview, popularity: $popularity, poster_path: $poster_path, production_companies: $production_companies, production_countries: $production_countries, release_date: $release_date, revenue: $revenue, runtime: $runtime, spoken_languages: $spoken_languages, status: $status, tagline: $tagline, title: $title, video: $video, vote_average: $vote_average, vote_count: $vote_count)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MovieModel &&
        other.adult == adult &&
        other.backdrop_path == backdrop_path &&
        other.belongs_to_collection == belongs_to_collection &&
        other.budget == budget &&
        listEquals(other.genre_ids, genre_ids) &&
        listEquals(other.genres, genres) &&
        other.homepage == homepage &&
        other.id == id &&
        other.imdb_id == imdb_id &&
        other.original_language == original_language &&
        other.original_title == original_title &&
        other.overview == overview &&
        other.popularity == popularity &&
        other.poster_path == poster_path &&
        listEquals(other.production_companies, production_companies) &&
        listEquals(other.production_countries, production_countries) &&
        other.release_date == release_date &&
        other.revenue == revenue &&
        other.runtime == runtime &&
        listEquals(other.spoken_languages, spoken_languages) &&
        other.status == status &&
        other.tagline == tagline &&
        other.title == title &&
        other.video == video &&
        other.vote_average == vote_average &&
        other.vote_count == vote_count;
  }

  @override
  int get hashCode {
    return adult.hashCode ^
        backdrop_path.hashCode ^
        belongs_to_collection.hashCode ^
        budget.hashCode ^
        genre_ids.hashCode ^
        genres.hashCode ^
        homepage.hashCode ^
        id.hashCode ^
        imdb_id.hashCode ^
        original_language.hashCode ^
        original_title.hashCode ^
        overview.hashCode ^
        popularity.hashCode ^
        poster_path.hashCode ^
        production_companies.hashCode ^
        production_countries.hashCode ^
        release_date.hashCode ^
        revenue.hashCode ^
        runtime.hashCode ^
        spoken_languages.hashCode ^
        status.hashCode ^
        tagline.hashCode ^
        title.hashCode ^
        video.hashCode ^
        vote_average.hashCode ^
        vote_count.hashCode;
  }
}
