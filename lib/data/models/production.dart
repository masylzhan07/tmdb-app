import 'dart:convert';

class ProductionCompanyModel {
  final int? id;
  final String? logo_path;
  final String? name;
  final String? origin_country;
  ProductionCompanyModel({
    this.id,
    this.logo_path,
    this.name,
    this.origin_country,
  });

  ProductionCompanyModel copyWith({
    int? id,
    String? logo_path,
    String? name,
    String? origin_country,
  }) {
    return ProductionCompanyModel(
      id: id ?? this.id,
      logo_path: logo_path ?? this.logo_path,
      name: name ?? this.name,
      origin_country: origin_country ?? this.origin_country,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'logo_path': logo_path,
      'name': name,
      'origin_country': origin_country,
    };
  }

  factory ProductionCompanyModel.fromMap(Map<String, dynamic> map) {
    return ProductionCompanyModel(
      id: map['id']?.toInt(),
      logo_path: map['logo_path'],
      name: map['name'],
      origin_country: map['origin_country'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductionCompanyModel.fromJson(String source) =>
      ProductionCompanyModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'ProductionCompanyModel(id: $id, logo_path: $logo_path, name: $name, origin_country: $origin_country)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductionCompanyModel &&
        other.id == id &&
        other.logo_path == logo_path &&
        other.name == name &&
        other.origin_country == origin_country;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        logo_path.hashCode ^
        name.hashCode ^
        origin_country.hashCode;
  }
}

class ProductionCountryModel {
  final String? iso_3166_1;
  final String? name;
  ProductionCountryModel({
    this.iso_3166_1,
    this.name,
  });

  ProductionCountryModel copyWith({
    String? iso_3166_1,
    String? name,
  }) {
    return ProductionCountryModel(
      iso_3166_1: iso_3166_1 ?? this.iso_3166_1,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'iso_3166_1': iso_3166_1,
      'name': name,
    };
  }

  factory ProductionCountryModel.fromMap(Map<String, dynamic> map) {
    return ProductionCountryModel(
      iso_3166_1: map['iso_3166_1'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProductionCountryModel.fromJson(String source) =>
      ProductionCountryModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'ProductionCountryModel(iso_3166_1: $iso_3166_1, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is ProductionCountryModel &&
        other.iso_3166_1 == iso_3166_1 &&
        other.name == name;
  }

  @override
  int get hashCode => iso_3166_1.hashCode ^ name.hashCode;
}
