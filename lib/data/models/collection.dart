import 'dart:convert';

class CollectionModel {
  final int? id;
  final String? name;
  final String? poster_path;
  final String? backdrop_path;
  CollectionModel({
    this.id,
    this.name,
    this.poster_path,
    this.backdrop_path,
  });

  CollectionModel copyWith({
    int? id,
    String? name,
    String? poster_path,
    String? backdrop_path,
  }) {
    return CollectionModel(
      id: id ?? this.id,
      name: name ?? this.name,
      poster_path: poster_path ?? this.poster_path,
      backdrop_path: backdrop_path ?? this.backdrop_path,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'poster_path': poster_path,
      'backdrop_path': backdrop_path,
    };
  }

  factory CollectionModel.fromMap(Map<String, dynamic> map) {
    return CollectionModel(
      id: map['id']?.toInt(),
      name: map['name'],
      poster_path: map['poster_path'],
      backdrop_path: map['backdrop_path'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CollectionModel.fromJson(String source) =>
      CollectionModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CollectionModel(id: $id, name: $name, poster_path: $poster_path, backdrop_path: $backdrop_path)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CollectionModel &&
        other.id == id &&
        other.name == name &&
        other.poster_path == poster_path &&
        other.backdrop_path == backdrop_path;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        poster_path.hashCode ^
        backdrop_path.hashCode;
  }
}
