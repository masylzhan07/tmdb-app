import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:tmdb_app/data/models/movie.dart';

class MoviesModel {
  final int page;
  final List<MovieModel>? results;
  final int total_pages;
  final int total_results;
  MoviesModel({
    required this.page,
    this.results,
    required this.total_pages,
    required this.total_results,
  });

  MoviesModel copyWith({
    int? page,
    List<MovieModel>? results,
    int? total_pages,
    int? total_results,
  }) {
    return MoviesModel(
      page: page ?? this.page,
      results: results ?? this.results,
      total_pages: total_pages ?? this.total_pages,
      total_results: total_results ?? this.total_results,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'page': page,
      'results': results?.map((x) => x.toMap()).toList(),
      'total_pages': total_pages,
      'total_results': total_results,
    };
  }

  factory MoviesModel.fromMap(Map<String, dynamic> map) {
    return MoviesModel(
      page: map['page']?.toInt() ?? 0,
      results: map['results'] != null
          ? List<MovieModel>.from(
              map['results']?.map((x) => MovieModel.fromMap(x)))
          : null,
      total_pages: map['total_pages']?.toInt() ?? 0,
      total_results: map['total_results']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory MoviesModel.fromJson(String source) =>
      MoviesModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'MoviesModel(page: $page, results: $results, total_pages: $total_pages, total_results: $total_results)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MoviesModel &&
        other.page == page &&
        listEquals(other.results, results) &&
        other.total_pages == total_pages &&
        other.total_results == total_results;
  }

  @override
  int get hashCode {
    return page.hashCode ^
        results.hashCode ^
        total_pages.hashCode ^
        total_results.hashCode;
  }
}
