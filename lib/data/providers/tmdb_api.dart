import 'package:dio/dio.dart';

class TMDBAPIRepository {
  late Dio _client;

  TMDBAPIRepository() {
    _client = Dio()
      ..options.baseUrl = 'https://api.themoviedb.org/3'
      ..options.queryParameters = {
        'api_key': '2c7cf565e780e67daaaf6f092c5f88eb'
      };
  }

  Future<Response> fetchTopMovies(int page) async {
    return await _client.get(
      '/movie/top_rated',
      queryParameters: {'page': page},
    );
  }

  Future<Response> fetchMovie(int id) async {
    return await _client.get('/movie/$id');
  }

  Future<Response> fetchSearched(int page, String srchTxt) async {
    Map<String, dynamic> params = {};
    params['page'] = page;
    if (srchTxt.isNotEmpty) {
      params['query'] = srchTxt;
    } else {
      params['query'] = '%';
    }
    return await _client.get(
      '/search/movie/',
      queryParameters: params,
    );
  }
}
