import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tmdb_app/constants/colors.dart';
import 'package:tmdb_app/constants/strings.dart';
import 'package:tmdb_app/data/models/movie.dart';
import 'package:tmdb_app/data/providers/tmdb_api.dart';
import 'package:tmdb_app/logic/bloc/allmovies_bloc.dart';
import 'package:tmdb_app/logic/cubit/navigation_cubit.dart';
import 'package:tmdb_app/logic/providers/movie_provider.dart';
import 'package:tmdb_app/widgets/page_error.dart';
import 'package:tmdb_app/widgets/page_loader.dart';

class AllMoviesScreen extends StatelessWidget {
  const AllMoviesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => AllMoviesBloc(
        repository: TMDBAPIRepository(),
      )..add(AllMoviesFetchEvent()),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          title: const Text(
            'Search Movie',
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: AllMoviesBody(),
      ),
    );
  }
}

class AllMoviesBody extends StatelessWidget {
  AllMoviesBody({Key? key}) : super(key: key);

  final List<MovieModel> _allMovies = [];
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: TextFormField(
            controller: _textEditingController,
            textCapitalization: TextCapitalization.sentences,
            autocorrect: false,
            decoration: InputDecoration(
              hintText: 'Search movie',
              isDense: true,
              contentPadding: const EdgeInsets.all(15),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: primaryColor),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: primaryColor),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: secondaryColor),
              ),
            ),
            onFieldSubmitted: (value) => {
              _allMovies.clear(),
              BlocProvider.of<AllMoviesBloc>(context)
                ..isFetching = true
                ..page = 1
                ..srchTxt = _textEditingController.text
                ..add(AllMoviesFetchEvent()),
            },
          ),
        ),
        Expanded(
          child: BlocConsumer<AllMoviesBloc, AllMoviesState>(
            listener: (context, allMoviesState) {
              if (allMoviesState is AllMoviesErrorState) {
                BlocProvider.of<AllMoviesBloc>(context).isFetching = false;
              }
              return;
            },
            builder: (context, allMoviesState) {
              if (allMoviesState is AllMoviesInitialState ||
                  allMoviesState is AllMoviesLoadingState &&
                      _allMovies.isEmpty) {
                return const CustomPageLoader();
              } else if (allMoviesState is AllMoviesSuccessState) {
                _allMovies.addAll(allMoviesState.allMovies);
                BlocProvider.of<AllMoviesBloc>(context).isFetching = false;
              } else if (allMoviesState is AllMoviesErrorState &&
                  _allMovies.isEmpty) {
                return const CustomPageError();
              }
              if (_allMovies.isEmpty) {
                return const Text('Nothing found');
              }
              return ListView(
                controller: _scrollController
                  ..addListener(() {
                    if (_scrollController.offset ==
                            _scrollController.position.maxScrollExtent &&
                        !BlocProvider.of<AllMoviesBloc>(context).isFetching) {
                      BlocProvider.of<AllMoviesBloc>(context)
                        ..isFetching = true
                        ..add(AllMoviesFetchEvent());
                    }
                    if (_scrollController.offset <=
                            _scrollController.position.minScrollExtent - 150 &&
                        !BlocProvider.of<AllMoviesBloc>(context).isFetching) {
                      _allMovies.clear();

                      BlocProvider.of<AllMoviesBloc>(context)
                        ..isFetching = true
                        ..page = 1
                        ..add(AllMoviesFetchEvent());
                    }
                  }),
                children: [
                  ListView.separated(
                    shrinkWrap: true,
                    physics: const ScrollPhysics(),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    itemBuilder: (context, index) =>
                        AllMoviesListItem(movie: _allMovies[index]),
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 40),
                    itemCount: _allMovies.length,
                  ),
                  Visibility(
                    visible:
                        !BlocProvider.of<AllMoviesBloc>(context).isEndOfList,
                    child: const SizedBox(
                      height: 40,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  )
                ],
              );
            },
          ),
        ),
      ],
    );
  }
}

class AllMoviesListItem extends StatelessWidget {
  final MovieModel movie;
  const AllMoviesListItem({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MovieProvider _movieProvider = Provider.of<MovieProvider>(context);

    return BlocBuilder<NavigationCubit, NavigationState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () {
            BlocProvider.of<NavigationCubit>(context).setTab(Tabs.movie);
            _movieProvider.setMovie(movie);
          },
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400]!,
                  offset: Offset(0, 4),
                  blurRadius: 15,
                ),
              ],
            ),
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      flex: 1,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.network(
                          '$smallImageUrl${movie.poster_path}',
                          fit: BoxFit.cover,
                          loadingBuilder: (context, widget, imageChunkEvent) {
                            return imageChunkEvent == null
                                ? widget
                                : const CircularProgressIndicator();
                          },
                        ),
                      ),
                    ),
                    const SizedBox(width: 20),
                    Flexible(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${movie.title}',
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          const SizedBox(height: 5),
                          Text(
                            'Original title: ${movie.original_title}',
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            'Release date: ${movie.release_date}',
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          Text(
                            'Vote average: ${movie.vote_average}',
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Text('${movie.overview}'),
              ],
            ),
          ),
        );
      },
    );
    // return ExpansionTile(
    //   title: Text('${movie.title}'),
    //   subtitle: Text('${movie.tagline}'),
    //   childrenPadding: const EdgeInsets.all(16),
    //   leading: Container(
    //     margin: EdgeInsets.only(top: 8),
    //     child: Text(movie.id.toString()),
    //   ),
    //   children: [
    //     Text(
    //       '${movie.overview}',
    //       textAlign: TextAlign.justify,
    //     ),
    //     const SizedBox(height: 20),
    // movie.poster_path == null
    //     ? Container()
    //     : Image.network(
    //         'https://api.themoviedb.org${movie.poster_path}',
    //         loadingBuilder: (context, widget, imageChunkEvent) {
    //           return imageChunkEvent == null
    //               ? widget
    //               : CircularProgressIndicator();
    //         },
    //         height: 300,
    //       ),
    //   ],
    // );
  }
}
