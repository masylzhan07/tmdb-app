import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tmdb_app/constants/strings.dart';
import 'package:tmdb_app/data/models/movie.dart';
import 'package:tmdb_app/data/repositories/movie_repository.dart';
import 'package:tmdb_app/logic/providers/movie_provider.dart';
import 'package:tmdb_app/widgets/page_error.dart';
import 'package:tmdb_app/widgets/page_loader.dart';

class MovieScreen extends StatelessWidget {
  const MovieScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MovieProvider _movieProvider = Provider.of<MovieProvider>(context);
    return Scaffold(
      body: _movieProvider.movie == null
          ? const Center(
              child: Text('Choose movie'),
            )
          : CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  expandedHeight: MediaQuery.of(context).size.height * 0.3,
                  pinned: true,
                  bottom: PreferredSize(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        '${_movieProvider.movie!.title}',
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    preferredSize: Size.fromHeight(30),
                  ),
                  flexibleSpace: Stack(
                    fit: StackFit.expand,
                    children: [
                      Image.network(
                        '$appbarImageUrl${_movieProvider.movie!.backdrop_path}',
                        fit: BoxFit.cover,
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Color.fromRGBO(0, 0, 0, 0),
                              Color.fromRGBO(0, 0, 0, 0.50),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      const SizedBox(height: 15),
                      FutureBuilder(
                        future: MovieRepository()
                            .fetchMovie(_movieProvider.movie!.id!),
                        builder: (context, AsyncSnapshot<MovieModel> snapshot) {
                          if (snapshot.hasData) {
                            String genres = snapshot.data!.genres!.fold('',
                                (previousValue, element) {
                              if (previousValue.isEmpty) {
                                return '${element.name}';
                              } else {
                                return '$previousValue, ${element.name}';
                              }
                            });
                            String companies = snapshot
                                .data!.production_companies!
                                .fold('', (previousValue, element) {
                              if (previousValue.isEmpty) {
                                return '${element.name}';
                              } else {
                                return '$previousValue, ${element.name}';
                              }
                            });
                            String countries = snapshot
                                .data!.production_countries!
                                .fold('', (previousValue, element) {
                              if (previousValue.isEmpty) {
                                return '${element.name}';
                              } else {
                                return '$previousValue, ${element.name}';
                              }
                            });
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${snapshot.data!.overview}',
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                  const SizedBox(height: 15),
                                  _movieInfo(
                                      'Original title: ${snapshot.data!.original_title}'),
                                  _movieInfo(
                                      'Release date: ${snapshot.data!.release_date}'),
                                  _movieInfo(
                                      'Vote average: ${snapshot.data!.vote_average}'),
                                  _movieInfo(
                                      'Budget: ${snapshot.data!.budget}'),
                                  _movieInfo('Genres: $genres'),
                                  _movieInfo(
                                      'Production companies: $companies'),
                                  _movieInfo(
                                      'Production countries: $countries'),
                                  _movieInfo(
                                      'Runtime: ${snapshot.data!.runtime} minutes'),
                                  const SizedBox(height: 100),
                                ],
                              ),
                            );
                          } else if (snapshot.hasError) {
                            return const SafeArea(child: CustomPageError());
                          }
                          return const SafeArea(child: CustomPageLoader());
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }

  Padding _movieInfo(String text) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
