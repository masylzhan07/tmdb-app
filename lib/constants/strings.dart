import 'package:flutter/material.dart';

enum Tabs { topMovies, movie, allMovies }
final navigatorKeys = {
  Tabs.topMovies: GlobalKey<NavigatorState>(),
  Tabs.movie: GlobalKey<NavigatorState>(),
  Tabs.allMovies: GlobalKey<NavigatorState>(),
};
const List<IconData> tabsIcons = [
  Icons.local_movies,
  Icons.movie,
  Icons.movie_filter,
];

const String smallImageUrl = 'https://image.tmdb.org/t/p/w300';
const String appbarImageUrl = 'https://image.tmdb.org/t/p/w500';
