import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tmdb_app/screens/all_movies_screen.dart';
import 'package:tmdb_app/screens/movie_screen.dart';
import 'package:tmdb_app/screens/top_movies_screen.dart';

import 'constants/colors.dart';
import 'constants/strings.dart';
import 'logic/cubit/navigation_cubit.dart';

//bottom navbar with 3 tabs
class CustomBottomNavigation extends StatefulWidget {
  static const routeName = '/bottom-nav';

  const CustomBottomNavigation({
    Key? key,
  }) : super(key: key);

  @override
  _CustomBottomNavigationState createState() => _CustomBottomNavigationState();
}

class _CustomBottomNavigationState extends State<CustomBottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationCubit, NavigationState>(
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () async {
            final isFirstRouteInCurrentTab =
                !await navigatorKeys[state.currentTab]!
                    .currentState!
                    .maybePop();
            if (isFirstRouteInCurrentTab) {
              // if not on the 'main' tab
              if (state.currentTab != Tabs.topMovies) {
                // select 'main' tab
                BlocProvider.of<NavigationCubit>(context).currentTab =
                    state.currentTab;
                // _selectTab(Tabs.topMovies);
                // back button handled by app
                return false;
              }
            }
            // let system handle back button if we're on the first route
            return isFirstRouteInCurrentTab;
          },
          child: Scaffold(
            extendBody: true,
            body: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                _buildOffstageNavigator(state, Tabs.topMovies),
                _buildOffstageNavigator(state, Tabs.movie),
                _buildOffstageNavigator(state, Tabs.allMovies),
              ],
            ),
            bottomNavigationBar: BottomNavigation(
              currentTab: state.currentTab,
              state: state,
            ),
          ),
        );
      },
    );
  }

//tab
  Widget _buildOffstageNavigator(NavigationState state, Tabs tabItem) {
    return Offstage(
      offstage: state.currentTab != tabItem,
      child: TabNavigator(
        navigatorKey: navigatorKeys[tabItem],
        tabItem: tabItem,
      ),
    );
  }
}

//bottom navbar
class BottomNavigation extends StatelessWidget {
  const BottomNavigation({
    Key? key,
    required this.currentTab,
    required this.state,
  }) : super(key: key);
  final Tabs currentTab;
  final NavigationState state;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(30),
        topRight: Radius.circular(30),
      ),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
        color: primaryColor,
        child: FittedBox(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ...Tabs.values
                    .asMap()
                    .map(
                      (key, tab) => MapEntry(
                        key,
                        currentTab == tab
                            ? Flexible(
                                child: IconButton(
                                  onPressed: () => navigatorKeys[tab]!
                                      .currentState!
                                      .popUntil((route) => route.isFirst),
                                  icon: Icon(
                                    tabsIcons[key],
                                    color: secondaryColor,
                                    size: 36,
                                  ),
                                ),
                              )
                            : Flexible(
                                child: IconButton(
                                  onPressed: () =>
                                      BlocProvider.of<NavigationCubit>(context)
                                          .setTab(tab),
                                  icon: Icon(
                                    tabsIcons[key],
                                    color: Colors.white,
                                    size: 36,
                                  ),
                                ),
                              ),
                      ),
                    )
                    .values
                    .toList(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TabNavigatorRoutes {
  static const String root = '/';
}

class TabNavigator extends StatelessWidget {
  const TabNavigator(
      {Key? key, required this.navigatorKey, required this.tabItem})
      : super(key: key);
  final GlobalKey<NavigatorState>? navigatorKey;
  final Tabs tabItem;

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, Tabs tab) {
    return {
      TabNavigatorRoutes.root: (context) {
        switch (tab) {
          case Tabs.topMovies:
            return const TopMoviesScreen();
          case Tabs.movie:
            return const MovieScreen();
          case Tabs.allMovies:
            return const AllMoviesScreen();
          default:
            return const TopMoviesScreen();
        }
      },
    };
  }

  @override
  Widget build(BuildContext context) {
    final routeBuilders = _routeBuilders(context, tabItem);

    return Navigator(
      key: navigatorKey,
      initialRoute: TabNavigatorRoutes.root,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
          builder: (context) => routeBuilders[routeSettings.name!]!(context),
        );
      },
    );
  }
}
