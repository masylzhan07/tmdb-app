import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:tmdb_app/constants/strings.dart';

part 'navigation_state.dart';

class NavigationCubit extends Cubit<NavigationState> {
  Tabs currentTab = Tabs.topMovies;
  NavigationCubit() : super(const NavigationState(Tabs.topMovies));

  void setTab(Tabs tab) {
    currentTab = tab;
    emit(NavigationState(tab));
  }
}
