part of 'navigation_cubit.dart';

class NavigationState extends Equatable {
  final Tabs currentTab;

  const NavigationState(this.currentTab);

  @override
  List<Object> get props => [currentTab];
}
