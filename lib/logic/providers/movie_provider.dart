import 'package:flutter/material.dart';
import 'package:tmdb_app/data/models/movie.dart';

class MovieProvider with ChangeNotifier {
  MovieModel? _movie;

  MovieModel? get movie => _movie;

  void setMovie(MovieModel movie) {
    _movie = movie;
    notifyListeners();
  }
}
