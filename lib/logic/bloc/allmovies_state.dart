part of 'allmovies_bloc.dart';

abstract class AllMoviesState {}

class AllMoviesInitialState extends AllMoviesState {
  AllMoviesInitialState();
}

class AllMoviesLoadingState extends AllMoviesState {
  AllMoviesLoadingState();
}

class AllMoviesSuccessState extends AllMoviesState {
  final List<MovieModel> allMovies;

  AllMoviesSuccessState({
    required this.allMovies,
  });
}

class AllMoviesErrorState extends AllMoviesState {
  final String error;

  AllMoviesErrorState({
    required this.error,
  });
}
