import 'package:bloc/bloc.dart';
import 'package:tmdb_app/data/models/movie.dart';
import 'package:tmdb_app/data/models/movies.dart';
import 'package:tmdb_app/data/providers/tmdb_api.dart';

part 'allmovies_event.dart';
part 'allmovies_state.dart';

class AllMoviesBloc extends Bloc<AllMoviesEvent, AllMoviesState> {
  final TMDBAPIRepository repository;
  int page = 1;
  String srchTxt = '';
  bool isFetching = false;
  bool isEndOfList = false;

  AllMoviesBloc({required this.repository}) : super(AllMoviesInitialState()) {
    on<AllMoviesEvent>((event, emit) async {
      if (event is AllMoviesFetchEvent) {
        emit(AllMoviesLoadingState());
        final response = await repository.fetchSearched(page, srchTxt);

        if (response.statusCode == 200) {
          final allMovies = MoviesModel.fromMap(response.data);
          if (allMovies.page <= allMovies.total_pages) {
            page++;
          } else {
            isEndOfList = true;
          }
          emit(AllMoviesSuccessState(
            allMovies: allMovies.results ?? [],
          ));
        } else {
          emit(AllMoviesErrorState(error: '${response.statusMessage}'));
        }
      }
    });
  }
}
