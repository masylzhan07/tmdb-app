import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:tmdb_app/data/models/movie.dart';
import 'package:tmdb_app/data/models/movies.dart';
import 'package:tmdb_app/data/providers/tmdb_api.dart';

part 'topmovies_event.dart';
part 'topmovies_state.dart';

class TopMoviesBloc extends Bloc<TopMoviesEvent, TopMoviesState> {
  final TMDBAPIRepository repository;
  int page = 1;
  bool isFetching = false;
  bool isEndOfList = false;

  TopMoviesBloc({required this.repository}) : super(TopMoviesInitialState()) {
    on<TopMoviesEvent>((event, emit) async {
      if (event is TopMoviesFetchEvent) {
        emit(TopMoviesLoadingState());
        final response = await repository.fetchTopMovies(page);

        if (response.statusCode == 200) {
          final topMovies = MoviesModel.fromMap(response.data);
          if (topMovies.page <= topMovies.total_pages) {
            page++;
          } else {
            isEndOfList = true;
          }
          emit(TopMoviesSuccessState(
            topMovies: topMovies.results ?? [],
          ));
        } else {
          emit(TopMoviesErrorState(error: '${response.statusMessage}'));
        }
      }
    });
  }
}
