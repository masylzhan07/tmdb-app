part of 'topmovies_bloc.dart';

@immutable
abstract class TopMoviesEvent {}

class TopMoviesFetchEvent extends TopMoviesEvent {
  TopMoviesFetchEvent();
}
