part of 'allmovies_bloc.dart';

abstract class AllMoviesEvent {}

class AllMoviesFetchEvent extends AllMoviesEvent {
  AllMoviesFetchEvent();
}
