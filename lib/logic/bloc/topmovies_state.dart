part of 'topmovies_bloc.dart';

@immutable
abstract class TopMoviesState {}

class TopMoviesInitialState extends TopMoviesState {
  TopMoviesInitialState();
}

class TopMoviesLoadingState extends TopMoviesState {
  TopMoviesLoadingState();
}

class TopMoviesSuccessState extends TopMoviesState {
  final List<MovieModel> topMovies;

  TopMoviesSuccessState({
    required this.topMovies,
  });
}

class TopMoviesErrorState extends TopMoviesState {
  final String error;

  TopMoviesErrorState({
    required this.error,
  });
}
